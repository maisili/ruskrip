{
  description = "ruskrip";

  outputs = { self }: {
    strok = {
      spici = "lamdy";
    };

    datom = { kor, bildRust, hyraizyn, rustModz }:

    {
      neim ? "ruskrip",
      skrip,
      vyrzyn ? (kor.cortHacString skrip),
      modzSet ? {},
      indeksCrates ? {},
      argz ? {},
    }@ruskripArgz:

    let
      inherit (builtins) hasAttr toFile;

      beisModz = { inherit (rustModz) nixrs; };

      hazIndeksCrates = hasAttr "indeksCrates" ruskripArgz;

      indeksCrates = {
        serde_json = { };
        async-std = { features = [ "default" "attributes" ]; };
      } //
      (kor.optionalAttrs hazIndeksCrates ruskripArgz.indeksCrates);

      skripMod = bildRust {
        neim = "skrip";
        inherit vyrzyn indeksCrates;
        modzSet = beisModz // (kor.optionalAttrs
        (hasAttr "modzSet" ruskripArgz) ruskripArgz.modset);
        iuniksSors = builtins.toFile
        "skrip-${neim}-${vyrzyn}.rs" skrip;
      };

      ruskripMainHac = kor.cortHacIuniks ./ruskrip.rs;

      ruskrip = bildRust {
        inherit neim indeksCrates;
        vyrzyn = ruskripMainHac;
        spici = "bin";

        modzSet = beisModz //
        { skrip = skripMod; };

        iuniksSors = builtins.toFile
        "ruskripMain-${ruskripMainHac}.rs"
        (builtins.readFile ./ruskrip.rs);
      };

      ruskripEksek = ruskrip.out + ruskrip.ryzylt;

    in derivation {
      name = neim;
      version = ruskripMainHac + vyrzyn;
      builder = ruskripEksek;
      inherit argz;
      system = hyraizyn.astra.sistym;
      __structuredAttrs = true;
    };

  };
}
