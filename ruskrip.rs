use {
  serde_json::Value,
  nixrs::get_attrs,
};

#[async_std::main]
async fn main() {
  let structured_attrs: Value = get_attrs().await.unwrap();
  let argz: &Value = &structured_attrs["argz"];
  skrip::ron(argz).await;
}
